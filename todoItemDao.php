<?php
interface TodoItemDao {
   public function save($todoItem);
   public function findAll();
   public function deleteAll();
   public function deleteById($id);
}