<?php

class MySqlTodoItemDao implements TodoItemDao {

    private $connection;

    function __construct() {
        $this->connection = new PDO("mysql:host=127.0.0.1;dbname=todoapp", "root", "HelloWorld12");
        $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    function save($todoItem) {
        $statement = $this->connection->prepare('insert into todo_item (name, date_added) values (:name, :date)');
        $statement->bindValue(':name', $todoItem->name);
        $statement->bindValue(':date', $todoItem->dateAdded);
        $statement->execute();
    }

    function findAll() {
        $statement = $this->connection->prepare('select id, name, date_added from todo_item');
        $statement->execute();

        $todoItems = [];
        foreach ($statement as $row) {
            $item = new TodoItem($row['name'], $row['date_added'], $row['id']);
            $todoItems[] = $item;
        }

        return $todoItems;
    }

    function deleteAll() {
        $statement = $this->connection->prepare('delete from todo_item');
        $statement->execute();
    }

    function deleteById($id) {
        $statement = $this->connection->prepare('delete from todo_item where id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
    }
}
