<?php
class TodoItem {
    public $id;
    public $name;
    public $dateAdded;
    public $comments = [];

    function __construct($name, $dateAdded, $id = null) {
        $this->name = $name;
        $this->dateAdded = $dateAdded;
        $this->id = $id;
    }

    function add_comment($comment) {
        if (isset($comment)) {
            $this->comments[] = $comment;
        }
    }
}