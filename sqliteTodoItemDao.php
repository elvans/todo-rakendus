<?php

class SqliteTodoItemDao implements TodoItemDao {

    const URL = "sqlite:todoApp.sqlite";

    private $connection;

    function __construct() {
        $this->connection = new PDO(self::URL);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    function save($todoItem) {
        $statement = $this->connection->prepare('insert into todo_item (name, date_added) values (:name, :date)');
        $statement->bindValue(':name', $todoItem->name);
        $statement->bindValue(':date', $todoItem->dateAdded);
        $statement->execute();
    }

    function findAll() {
        $statement = $this->connection->prepare('
          select * from todo_item 
          left join comment on comment.todo_item_id = todo_item.id;');
        $statement->execute();

        $todoItems = [];
        foreach ($statement as $row) {
            $id = $row['id'];
            if (isset($todoItems[$id])) {
               // todoitem on juba olemas, lisame sellele kommentaari
                $item = $todoItems[$id];
                $item->add_comment($row['content']);
            } else {
               // uus todoitem
                $item = new TodoItem($row['name'], $row['date_added'], $row['id']);
                $item->add_comment($row['content']);
                $todoItems[$item->id] = $item;
            }
        }

        return $todoItems;
    }

    function deleteAll() {
        $statement = $this->connection->prepare('delete from todo_item');
        $statement->execute();
    }

    function deleteById($id) {
        $statement = $this->connection->prepare('delete from todo_item where id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
    }
}
