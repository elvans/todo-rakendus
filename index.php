<?php
require_once("todoItemDao.php");
require_once("todoItem.php");
require_once("sqliteTodoItemDao.php");
require_once("lib/tpl.php");

$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "view";
session_start();
$todoItemDao = new SqliteTodoItemDao();

function validate_todo_item($item) {
    $errorMessages = [];

    if (empty($item)) {
        $errorMessages[] = "Todo item ei või olla tühi!";
    }

    if (strlen($item) < 3) {
        $errorMessages[] = "Pikkus peab olema vähemalt 3 tähemärki";
    }

    if (strlen($item) > 20) {
        $errorMessages[] = "Pikku ei tohi olla suurem kui 20 tähemärki";
    }

    return $errorMessages;
}

if ($cmd == "view") {

    $todoItems = $todoItemDao->findAll();
    $data = ['$todoItems' => $todoItems];

    if (isset($_GET["message"])) {
        $data['$message'] = $_GET["message"];
    }

    print render_template("tpl/main.html", $data);

} else if ($cmd == "add") {
    $data = [];

    $todoItemName = $_POST["todoItem"];
    if (isset($todoItemName)) {
        $errors = validate_todo_item($todoItemName);
        if (count($errors) == 0) {
            $todoItem = new TodoItem($todoItemName, date("Y-m-d"));
            $todoItemDao->save($todoItem);
            header("Location: ?cmd=view");
        } else {
            $data['$errors'] = $errors;
            $data['$todoItems'] = get_todo_items();
            print render_template("tpl/main.html", $data);
        }
    }

} else if ($cmd == "delete") {

    if (isset($_POST["id"])) {
        $todoItemDao->deleteById($_POST["id"]);
    }

    header("Location: ?cmd=view");

} else if ($cmd == "delete_all") {
    if (isset($_POST["delete-all"])) {
        $todoItemDao->deleteAll();
    }

    $message = urlencode("Kõik kustutatud");
    header("Location: ?cmd=view&message=$message");
}





